from sqlalchemy import create_engine, MetaData
from sqlalchemy.orm import sessionmaker, scoped_session


class DataBase:
    engine = None
    metadata = MetaData()
    session = scoped_session(sessionmaker(autocommit=False, autoflush=False))

    def init(self, conn_string: str):
        self.engine = create_engine(conn_string)
        self.import_mappers()
        self.metadata.create_all(bind=self.engine)
        self.session = self.session.configure(bind=self.engine)

    def import_mappers(self):
        from app.domain_model import comments


db = DataBase()
