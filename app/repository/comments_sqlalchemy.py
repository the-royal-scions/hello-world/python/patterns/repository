from app.domain_model.comments.model import CommentsModel
from .interface import RepositoryInterface
from app.extensions.database import db


class CommentsSqlalchemyRepository(RepositoryInterface):
    def __init__(self, session=db.session):
        self._session = session

    def add(self, comment: CommentsModel):
        self._session.add(comment)
        self._session.commit()

    def get_by(self, uuid: str):
        return self._session.query(CommentsModel).filter_by(uuid=uuid).one()

    def get_all(self):
        return self._session.query(CommentsModel).all()

    def delete(self, uuid: str):
        comment = self.get_by(uuid)
        self._session.delete(comment)
        self._session.commit()
