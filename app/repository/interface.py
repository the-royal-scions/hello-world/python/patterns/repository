import abc
from typing import List


class RepositoryInterface(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def add(self, object: object) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def get_by(self, **kwargs) -> object:
        raise NotImplementedError

    @abc.abstractmethod
    def get_all(self) -> List[object]:
        raise NotImplementedError

    @abc.abstractmethod
    def delete(self, **kwargs) -> None:
        raise NotImplementedError
