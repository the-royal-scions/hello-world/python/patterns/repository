from .interface import RepositoryInterface
from app.domain_model.comments.model import CommentsModel


class CommentsInCSVRepository(RepositoryInterface):
    def __init__(self, file_path: str):
        self._file_path = file_path

    def add(self, comment: CommentsModel):
        if not isinstance(comment, CommentsModel):
            raise ValueError

        with open(self._file_path, "a") as _file:
            line = f"{comment.uuid};{comment.comment}\n"
            _file.write(line)

    def get_by(self, uuid: str):
        for row in open(self._file_path, "r"):
            if row.find(uuid):
                data = row.strip().split(";")
                comment = CommentsModel(data[1])
                comment.uuid = data[0]
                return comment

    def get_all(self):
        comments = []
        for row in open(self._file_path, "r"):
            data = row.strip().split(";")
            comment = CommentsModel(data[1])
            comment.uuid = data[0]
            comments.append(comment)
        return comments

    def delete(self, uuid: str):
        with open(self._file_path, "r+") as _file:
            lines = _file.readlines()
            _file.seek(0)
            for row in lines:
                if row.find(uuid):
                    _file.write(row)
            _file.truncate()
