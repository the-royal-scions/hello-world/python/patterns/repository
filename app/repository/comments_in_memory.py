from .interface import RepositoryInterface
from app.domain_model.comments.model import CommentsModel


class CommentsInMemoryRepository(RepositoryInterface):
    def __init__(self):
        self._comments = list()

    def add(self, comment: CommentsModel):
        if not isinstance(comment, CommentsModel):
            raise ValueError
        self._comments.append(comment)

    def get_by(self, uuid: str):
        return next(c for c in self._comments if c.uuid == uuid)

    def get_all(self):
        return self._comments

    def delete(self, uuid: str):
        comment = self.get_by(uuid=uuid)
        index = self._comments.index(comment)
        del self._comments[index]
