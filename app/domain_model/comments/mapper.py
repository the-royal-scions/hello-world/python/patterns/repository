from sqlalchemy.orm import mapper
from .model import CommentsModel
from .table import comments_table

mapper(CommentsModel, comments_table)
