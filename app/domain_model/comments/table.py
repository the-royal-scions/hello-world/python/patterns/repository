from sqlalchemy import Table, Column, String
from app.extensions.database import db


comments_table = Table(
    'comments', db.metadata,
    Column('uuid', String(36), primary_key=True),
    Column('comment', String(255), nullable=False),
)
