import uuid


class CommentsModel:
    def __init__(self, comment: str):
        self.uuid = str(uuid.uuid4())
        self.comment = comment

    def __repr__(self):
        return f"CommentsModel(comment={self.comment})"

    def __str__(self):
        return f"uuid: {self.uuid}\ncomment: {self.comment}"
