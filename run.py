from app.domain_model.comments.table import comments_table
from app.domain_model.comments.model import CommentsModel
from app.repository.comments_in_memory import CommentsInMemoryRepository
from app.repository.comments_in_csv import CommentsInCSVRepository
from app.repository.comments_sqlalchemy import CommentsSqlalchemyRepository
from app.extensions.database import db


db.init("sqlite:///db.sqlite")


comment_1 = CommentsModel("Primeiro Comentário")
comment_2 = CommentsModel("Segundo Comentário")

# comments_repository = CommentsInMemoryRepository()
# comments_repository = CommentsInCSVRepository("db.csv")
comments_repository = CommentsSqlalchemyRepository()

comments_repository.add(comment_1)
comments_repository.add(comment_2)

for comment in comments_repository.get_all():
    print(comment)
